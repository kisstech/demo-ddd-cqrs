package com.kiss.template.simpledddcqrs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleDddCqrsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleDddCqrsApplication.class, args);
    }

}
