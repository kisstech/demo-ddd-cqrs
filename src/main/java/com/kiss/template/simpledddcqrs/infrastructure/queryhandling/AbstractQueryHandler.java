package com.kiss.template.simpledddcqrs.infrastructure.queryhandling;


public abstract class AbstractQueryHandler<Q extends Query<R>, R extends Result> {

	public abstract R handle(Q query);

	public abstract Class<?> getQueryType();

}
