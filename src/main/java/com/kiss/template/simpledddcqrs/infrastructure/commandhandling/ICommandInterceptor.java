package com.kiss.template.simpledddcqrs.infrastructure.commandhandling;

/**
 * Interface for a command interception mechanism, allowing to execute some
 * behavior before the command is effectively handled.
 */
public interface ICommandInterceptor {

	/**
	 * Dispatches a command to the appropriate handler.
	 * 
	 * @param command
	 *            the command to execute
	 */
	void handle(Command command);

}