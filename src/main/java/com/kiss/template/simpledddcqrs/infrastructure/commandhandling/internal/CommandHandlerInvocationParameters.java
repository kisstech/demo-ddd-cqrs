package com.kiss.template.simpledddcqrs.infrastructure.commandhandling.internal;

import java.lang.reflect.Method;

public class CommandHandlerInvocationParameters {

	public Object object;
	public Method method;
	public Object argument;

	public CommandHandlerInvocationParameters(Object object, Method method, Object argument) {
		this.object = object;
		this.method = method;
		this.argument = argument;
	}

}
