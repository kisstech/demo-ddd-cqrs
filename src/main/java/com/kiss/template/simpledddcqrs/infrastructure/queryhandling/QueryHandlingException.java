package com.kiss.template.simpledddcqrs.infrastructure.queryhandling;

public class QueryHandlingException extends RuntimeException {

	private static final long serialVersionUID = -3160862213158444888L;

	public QueryHandlingException(Exception e) {
		super(e);
	}

}
