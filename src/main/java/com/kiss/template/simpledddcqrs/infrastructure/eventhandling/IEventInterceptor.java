package com.kiss.template.simpledddcqrs.infrastructure.eventhandling;


/**
 * Interface for an event interception mechanism, allowing to execute some
 * behavior before the event is effectively handled.
 */
public interface IEventInterceptor {

	void handle(Event event);

}