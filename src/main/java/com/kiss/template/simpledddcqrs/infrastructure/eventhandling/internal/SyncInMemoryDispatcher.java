package com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.Event;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.EventHandlerInfo;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.IEventDispatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


@Component
public class SyncInMemoryDispatcher implements IEventDispatcher {

	private static final Logger LOGGER = LoggerFactory.getLogger(SyncInMemoryDispatcher.class);

	private final ConcurrentMap<Class<?>, Set<EventHandlerInfo>> registeredEventHandlers = new ConcurrentHashMap<Class<?>, Set<EventHandlerInfo>>();

	@Override
	public void publish(Event event) {
		Set<EventHandlerInfo> eventHandlerInfos = getHandlers(event);
		for (EventHandlerInfo eventHandlerInfo : eventHandlerInfos) {
			EventHandlerInvocationParameters invocationParameters = new EventHandlerInvocationParameters(eventHandlerInfo.getSubscriber(),
					eventHandlerInfo.getHandlerMethod(), event);
			LOGGER.debug("Dispatching event " + event + " to " + eventHandlerInfo.getSubscriber() + "."
					+ eventHandlerInfo.getHandlerMethod().getName());
			new EventHandlerInvoker().invoke(invocationParameters);
		}

	}

	@Override
	public void register(Class<?> eventClass, EventHandlerInfo info) {
		Set<EventHandlerInfo> registered = registeredEventHandlers.get(eventClass);
		if (registered == null) {
			registered = new HashSet<>();
			LOGGER.debug("Registering handler " + info.getSubscriber() + "." + info.getHandlerMethod().getName() + " for event type "
					+ eventClass.getName());
			registeredEventHandlers.put(eventClass, registered);
		}
		registered.add(info);
	}

	private Set<EventHandlerInfo> getHandlers(Event event) {
		Set<EventHandlerInfo> eventHandlers = registeredEventHandlers.get(event.getClass());
		return eventHandlers != null ? eventHandlers : new HashSet<>();
	}

}
