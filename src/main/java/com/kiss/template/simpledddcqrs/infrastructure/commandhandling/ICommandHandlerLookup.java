package com.kiss.template.simpledddcqrs.infrastructure.commandhandling;


import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.internal.CommandHandlerInfo;

import java.util.Map;

/**
 * Interface for a strategy to find command registrations made by a given object
 * (the subscriber).
 */
public interface ICommandHandlerLookup {

	public Map<Class<?>, CommandHandlerInfo> findHandlers(Object subscriber);

}