package com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal;


import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.Event;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.EventHandlerInfo;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.IEventDispatcher;

public class AsyncInMemoryDispatcher implements IEventDispatcher {

	@Override
	public void publish(Event event) {
		// Not yet implemented
	}

	@Override
	public void register(Class<?> eventClass, EventHandlerInfo info) {
		throw new UnsupportedOperationException("Async event dispatching is not yet implemented");
	}

}
