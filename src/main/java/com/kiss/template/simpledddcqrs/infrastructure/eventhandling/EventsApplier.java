package com.kiss.template.simpledddcqrs.infrastructure.eventhandling;


import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal.EventHandlerInvocationParameters;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal.EventHandlerInvoker;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class EventsApplier {

	private IEventHandlerLookup lookup;

	public EventsApplier(IEventHandlerLookup lookup) {
		this.lookup = lookup;
	}

	public void applyEvents(Object stateObject, List<? extends Event> events) {
		Map<Class<?>, Set<EventHandlerInfo>> synchronousEventHandlers = lookup.findSynchronousHandlers(stateObject);
		for (Event event : events) {
			if (synchronousEventHandlers.containsKey(event.getClass())) {
				Set<EventHandlerInfo> eventHandlerInfos = synchronousEventHandlers.get(event.getClass());
				for (EventHandlerInfo eventHandlerInfo : eventHandlerInfos) {
					EventHandlerInvocationParameters invocationParameters = new EventHandlerInvocationParameters(eventHandlerInfo.getSubscriber(),
							eventHandlerInfo.getHandlerMethod(), event);
					new EventHandlerInvoker().invoke(invocationParameters);
				}

			}
		}
	}
}
