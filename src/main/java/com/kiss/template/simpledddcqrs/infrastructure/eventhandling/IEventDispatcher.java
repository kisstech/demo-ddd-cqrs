package com.kiss.template.simpledddcqrs.infrastructure.eventhandling;

/**
 * Interface for an event dispatcher, in charge of dispatching events to events
 * handlers.
 */
public interface IEventDispatcher {

	public void publish(Event event);

	public void register(Class<?> eventClass, EventHandlerInfo info);

}
