package com.kiss.template.simpledddcqrs.infrastructure.commandhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.ICommandDispatcher;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.ICommandHandlerLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class CommandDispatcher implements ICommandDispatcher {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommandDispatcher.class);

	private final ConcurrentMap<Class<?>, CommandHandlerInfo> commandHandlers = new ConcurrentHashMap<Class<?>, CommandHandlerInfo>();
	private ICommandHandlerLookup commandHandlerLookup;

	public CommandDispatcher(ICommandHandlerLookup commandHandlerLookup) {
		LOGGER.info("Initializing comment dispatcher");
		this.commandHandlerLookup = commandHandlerLookup;
	}

	@Override
	public void dispatch(Object command) {
		LOGGER.debug("Publishing command " + command);
		CommandHandlerInfo commandHandlerInfo = getHandler(command);
		CommandHandlerInvocationParameters invocationParameters = new CommandHandlerInvocationParameters(commandHandlerInfo.getSubscriber(),
				commandHandlerInfo.getHandlerMethod(), command);
		LOGGER.info("Dispatching command " + command + " to " + commandHandlerInfo.getSubscriber() + "."
				+ commandHandlerInfo.getHandlerMethod().getName());
		new SimpleCommandHandlerInvoker().invoke(invocationParameters);
	}

	@Override
	public void register(Object subscriber) {
		Map<Class<?>, CommandHandlerInfo> eventHandlers = commandHandlerLookup.findHandlers(subscriber);
		for (Entry<Class<?>, CommandHandlerInfo> commandHandlingRegistration : eventHandlers.entrySet()) {
			if (commandHandlers.get(commandHandlingRegistration.getKey()) != null) {
				throw new InvalidParameterException("Multiple handlers declarations found for command " + commandHandlingRegistration.getKey());
			}
			commandHandlers.put(commandHandlingRegistration.getKey(), commandHandlingRegistration.getValue());
		}
	}

	@Override
	public Set<Class<?>> getCommands() {
		return commandHandlers.keySet();
	}

	private CommandHandlerInfo getHandler(Object message) {
		CommandHandlerInfo commandHandlerInfo = commandHandlers.get(message.getClass());
		if (commandHandlerInfo == null) {
			throw new InvalidParameterException("No handler configured for command type " + message.getClass());
		}
		return commandHandlerInfo;
	}

}