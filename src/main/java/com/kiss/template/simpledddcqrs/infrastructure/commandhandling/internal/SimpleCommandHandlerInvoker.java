package com.kiss.template.simpledddcqrs.infrastructure.commandhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.CommandHandlingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Performs a method invocation using provided parameters. Failures are wrapped
 * in an unchecked EventHandlingException.
 */
public class SimpleCommandHandlerInvoker {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleCommandHandlerInvoker.class);

	public void invoke(CommandHandlerInvocationParameters invocationParameters) {
		LOGGER.debug("Invoking method " + invocationParameters.method.getName() + " of object " + invocationParameters.object + " with message "
				+ invocationParameters.argument);

		try {
			invocationParameters.method.invoke(invocationParameters.object, invocationParameters.argument);
		} catch (IllegalArgumentException e) {
			throw new CommandHandlingException(e);
		} catch (IllegalAccessException e) {
			throw new CommandHandlingException(e);
		} catch (InvocationTargetException e) {
			throw new CommandHandlingException(e.getTargetException());
		}
	}

}
