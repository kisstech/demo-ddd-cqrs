package com.kiss.template.simpledddcqrs.infrastructure.commandhandling;

import java.util.Set;

/**
 * Interface for a command dispatching mechanism, allowing to execute actions
 * regardless of the component actually handling it.
 */
public interface ICommandDispatcher {

	/**
	 * Dispatches a command to the appropriate handler.
	 * 
	 * @param command
	 *            the command to execute
	 */
	void dispatch(Object command);

	/**
	 * Subscribes the object to events dispatched on this bus.
	 * 
	 * @param subscriber
	 *            the subscribing object
	 */
	void register(Object subscriber);

	public Set<Class<?>> getCommands();

}