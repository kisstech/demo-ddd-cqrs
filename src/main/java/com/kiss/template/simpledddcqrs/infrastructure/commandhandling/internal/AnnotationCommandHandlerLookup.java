package com.kiss.template.simpledddcqrs.infrastructure.commandhandling.internal;


import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.CommandHandler;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.ICommandHandlerLookup;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Strategy to find command registrations made by a given object (the
 * subscriber). The command handling methods are considered to be the one
 * declaring the CommandHandler annotation, and taking the command class as a
 * parameter.
 */
@Component
public class AnnotationCommandHandlerLookup implements ICommandHandlerLookup {

	@Override
	public Map<Class<?>, CommandHandlerInfo> findHandlers(Object subscriber) {
		Map<Class<?>, CommandHandlerInfo> handlers = new HashMap<Class<?>, CommandHandlerInfo>();
		for (Method m : subscriber.getClass().getDeclaredMethods()) {
			for (Annotation annotation : m.getAnnotations()) {
				if (annotation.annotationType().isAssignableFrom(CommandHandler.class)) {
					Class<?> eventClass = m.getParameterTypes()[0];
					CommandHandlerInfo commandHandlerInfo = new CommandHandlerInfo(subscriber, m);
					handlers.put(eventClass, commandHandlerInfo);
				}
			}
		}
		return handlers;
	}

}
