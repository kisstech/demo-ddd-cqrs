package com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.EventHandlingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;

/**
 * Performs a method invocation using provided parameters. Failures are wrapped
 * in an unchecked EventHandlingException.
 * 
 */
public class EventHandlerInvoker {

	private static final Logger LOGGER = LoggerFactory.getLogger(EventHandlerInvoker.class);

	public void invoke(EventHandlerInvocationParameters invocationParameters) {
		LOGGER.debug("Invoking method " + invocationParameters.method.getName() + " of object " + invocationParameters.object + " with message "
				+ invocationParameters.argument);

		try {
			invocationParameters.method.invoke(invocationParameters.object, invocationParameters.argument);
		} catch (IllegalArgumentException e) {
			throw new EventHandlingException(e);
		} catch (IllegalAccessException e) {
			throw new EventHandlingException(e);
		} catch (InvocationTargetException e) {
			throw new EventHandlingException(e.getTargetException());
		}
	}

}
