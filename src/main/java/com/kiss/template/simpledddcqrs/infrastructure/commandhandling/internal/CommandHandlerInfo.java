package com.kiss.template.simpledddcqrs.infrastructure.commandhandling.internal;

import java.lang.reflect.Method;

public class CommandHandlerInfo {

	private Object subscriber;
	private Method handlerMethod;

	public CommandHandlerInfo(Object subscriber, Method handlerMethod) {
		this.subscriber = subscriber;
		this.handlerMethod = handlerMethod;
	}

	public Object getSubscriber() {
		return subscriber;
	}

	public Method getHandlerMethod() {
		return handlerMethod;
	}

}
