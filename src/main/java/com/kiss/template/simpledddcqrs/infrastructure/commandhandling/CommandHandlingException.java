package com.kiss.template.simpledddcqrs.infrastructure.commandhandling;

/**
 * Exception thrown in the case of an error occurring during the invocation of
 * an event handling method.
 */
public class CommandHandlingException extends RuntimeException {

	private static final long serialVersionUID = 8939133363877357533L;

	public CommandHandlingException(String message) {
		super(message);
	}

	public CommandHandlingException(Throwable e) {
		super(e);
	}

}
