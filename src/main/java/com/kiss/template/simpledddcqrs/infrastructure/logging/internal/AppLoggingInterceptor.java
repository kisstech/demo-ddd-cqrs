package com.kiss.template.simpledddcqrs.infrastructure.logging.internal;

import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.Command;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.ICommandInterceptor;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.Event;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.IEventInterceptor;
import com.kiss.template.simpledddcqrs.infrastructure.logging.AppLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import static com.kiss.template.simpledddcqrs.infrastructure.logging.AppLog.LogEntryType.COMMAND;
import static com.kiss.template.simpledddcqrs.infrastructure.logging.AppLog.LogEntryType.EVENT;

@Component
public class AppLoggingInterceptor implements ICommandInterceptor, IEventInterceptor {

	@Autowired
	private AppLog appLog;

	public void clear() {
		appLog.clear();
	}

	public List<Command> getCommands() {
		return appLog.getCommands();
	}

	@Override
	public void handle(Command cmd) {
		appLog.add(generateNewIdentifier(), new AppLog.LogEntry(cmd, COMMAND));
	}

	@Override
	public void handle(Event event) {
		appLog.add(generateNewIdentifier(), new AppLog.LogEntry(event, EVENT));
	}

	private String generateNewIdentifier() {
		return UUID.randomUUID().toString();
	}

	public Collection<AppLog.LogEntry> getAllEntries() {
		return appLog.getAllEntries();
	}

}
