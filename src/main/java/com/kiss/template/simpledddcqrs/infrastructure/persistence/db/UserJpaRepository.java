package com.kiss.template.simpledddcqrs.infrastructure.persistence.db;

import com.kiss.template.simpledddcqrs.domain.user.User;
import com.kiss.template.simpledddcqrs.domain.user.UserRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserJpaRepository extends UserRepository, CrudRepository<User, Long> {

    @Override
    User save(User user);

    @Override
    List<User> findAll();

    @Override
    User findByLogin(String login);
}
