package com.kiss.template.simpledddcqrs.infrastructure.queryhandling;

/**
 * Interface for a query dispatching mechanism, allowing to execute queries
 * regardless of the component actually handling it.
 */
public interface IQueryDispatcher {

	/**
	 * Dispatches a query to the appropriate handler.
	 * 
	 * @param query
	 *            the query to execute
	 */
	<R extends Result, Q extends Query<R>> R dispatch(Query<R> query);

	/**
	 * Register the object as the handler for a query class.
	 * 
	 * @param handler
	 *            the handler object
	 */
	void register(AbstractQueryHandler<?, ?> handler);

}