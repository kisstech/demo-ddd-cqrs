package com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Simple implementation of an in-memory, JVM-local event bus. Event handlers
 * are detected on subscribing objects using a {@link IEventHandlerLookup}.
 * Published events will be dispatched to each subscriber using a
 * {@link IEventHandlerInvoker}.
 * 
 * Please note that there is no guarantee regarding the order in which handlers
 * of a given event type are being dispatched an event of that type.
 * 
 */
@Component
public class EventBus implements IEventBus {

	private static final Logger LOGGER = LoggerFactory.getLogger(EventBus.class);

	private final IEventDispatcher syncDispatcher;
	private final IEventDispatcher asyncDispatcher;

	private final IEventHandlerLookup eventHandlerLookup;

	public EventBus(IEventHandlerLookup subscriptionsLookup, IEventDispatcher syncDispatcher, IEventDispatcher asyncDispatcher) {
		LOGGER.info("Initializing event bus");
		this.eventHandlerLookup = subscriptionsLookup;
		this.syncDispatcher = syncDispatcher;
		this.asyncDispatcher = asyncDispatcher;
	}

	@Override
	public void publish(Event event) {
		LOGGER.debug("Publishing event " + event);
		syncDispatcher.publish(event);
//FIXME manage async event		asyncDispatcher.publish(event);
	}

	@Override
	public void subscribe(Object subscriber) {
		subscribeSynchronousHandlers(subscriber);
		subscribeAsynchronousHandlers(subscriber);
	}

	private void subscribeAsynchronousHandlers(Object subscriber) {
		Map<Class<?>, Set<EventHandlerInfo>> asynchrounousEventHandlers = eventHandlerLookup.findAsynchronousHandlers(subscriber);
		for (Entry<Class<?>, Set<EventHandlerInfo>> eventSubscriptions : asynchrounousEventHandlers.entrySet()) {
			registerAsynchronousHandlers(eventSubscriptions.getKey(), eventSubscriptions.getValue());
		}
	}

	private void subscribeSynchronousHandlers(Object subscriber) {
		Map<Class<?>, Set<EventHandlerInfo>> synchronousEventHandlers = eventHandlerLookup.findSynchronousHandlers(subscriber);
		for (Entry<Class<?>, Set<EventHandlerInfo>> eventSubscriptions : synchronousEventHandlers.entrySet()) {
			registerSynchronousHandlers(eventSubscriptions.getKey(), eventSubscriptions.getValue());
		}
	}

	private void registerAsynchronousHandlers(Class<?> eventClass, Set<EventHandlerInfo> eventHandlers) {
		for (EventHandlerInfo info : eventHandlers) {
			asyncDispatcher.register(eventClass, info);
		}
	}

	private void registerSynchronousHandlers(Class<?> eventClass, Set<EventHandlerInfo> eventHandlers) {
		for (EventHandlerInfo info : eventHandlers) {
			syncDispatcher.register(eventClass, info);
		}
	}
}