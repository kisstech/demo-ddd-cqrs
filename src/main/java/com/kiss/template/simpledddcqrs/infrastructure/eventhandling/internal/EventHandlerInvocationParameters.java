package com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal;


import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.Event;

import java.lang.reflect.Method;

public class EventHandlerInvocationParameters {

	public Event argument;
	public Method method;
	public Object object;

	public EventHandlerInvocationParameters(Object object, Method method, Event argument) {
		this.object = object;
		this.method = method;
		this.argument = argument;
	}

}
