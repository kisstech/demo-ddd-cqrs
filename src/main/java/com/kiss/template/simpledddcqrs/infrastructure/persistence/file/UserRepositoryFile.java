package com.kiss.template.simpledddcqrs.infrastructure.persistence.file;

import com.kiss.template.simpledddcqrs.domain.user.User;
import com.kiss.template.simpledddcqrs.domain.user.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;

/**
 * repository implementation writing to a file
 */
@Component
@Profile("file")
public class UserRepositoryFile implements UserRepository {

    @Override
    public User save(User user) {
        try {
            Files.write(new File("users").toPath(),
                    user.toString().getBytes(),
                    StandardOpenOption.CREATE,
                    StandardOpenOption.APPEND);
        } catch (IOException e) {
            //TODO manage error
            e.printStackTrace();
        }finally {
            return user;
        }
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public User findByLogin(String login) {
        return User.builder().login("login").build();
    }
}
