package com.kiss.template.simpledddcqrs.infrastructure.logging;


import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.Command;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.Event;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * TODO Persist app log
 */
@Component
public class AppLog implements Serializable {

	private static final long serialVersionUID = 3792641799104322277L;

	public enum LogEntryType {
		COMMAND, EVENT
	};

	public static class LogEntry implements Serializable {

		private static final long serialVersionUID = -7830251018324740740L;

		private Object payload;
		private LogEntryType logEntryType;

		public LogEntry(Object payload, LogEntryType messageType) {
			this.payload = payload;
			this.logEntryType = messageType;
		}

		public Object getPayload() {
			return payload;
		}

		public LogEntryType getLogEntryType() {
			return logEntryType;
		}
		
		@Override
		public String toString() {
			return payload.toString();
		}
	}

	private final LinkedHashMap<String, LogEntry> entries = new LinkedHashMap<>();

	public Collection<LogEntry> getAllEntries() {
		return entries.values();
	}

	public void clear() {
		entries.clear();
	}
	
	public List<Command> getCommands() {
		List<Command> commands = new ArrayList<>();
		for (LogEntry entry : entries.values()) {
			if (entry.logEntryType == LogEntryType.COMMAND) {
				commands.add((Command) entry.payload);
			}
		}
		return commands;
	}

	public List<Event> getEvents() {
		List<Event> events = new ArrayList<>();
		for (LogEntry entry : entries.values()) {
			if (entry.logEntryType == LogEntryType.EVENT) {
				events.add((Event) entry.payload);
			}
		}
		return events;
	}

	public void add(String id, LogEntry message) {
		entries.put(id, message);
	}
	
}
