package com.kiss.template.simpledddcqrs.infrastructure.queryhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class QueryDispatcher implements IQueryDispatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryDispatcher.class);

    private final ConcurrentMap<Class<?>, AbstractQueryHandler<?, ?>> queryHandlers = new ConcurrentHashMap<>();

    @SuppressWarnings("unchecked")
    @Override
    public <R extends Result, Q extends Query<R>> R dispatch(Query<R> query) {
        AbstractQueryHandler<Q, R> queryHandler = (AbstractQueryHandler<Q, R>) getHandler(query);
        LOGGER.info("Dispatching query " + query + " to " + queryHandler);
        try {
            return queryHandler.handle((Q) query);
        } catch (Exception e) {
            throw new QueryHandlingException(e);
        }
    }

    @Override
    public void register(AbstractQueryHandler<?, ?> queryHandler) {
        LOGGER.debug("Registering queryHandler " + queryHandler + " to handle queries of type " + queryHandler.getQueryType());
        queryHandlers.put(queryHandler.getQueryType(), queryHandler);
    }

    @SuppressWarnings("unchecked")
    public <R extends Result, Q extends Query<R>> AbstractQueryHandler<Q, R> getHandler(Q query) {
        AbstractQueryHandler<Q, R> queryHandler = (AbstractQueryHandler<Q, R>) queryHandlers.get(query.getClass());
        if (queryHandler == null) {
            throw new InvalidParameterException("No handler configured for query type " + query.getClass());
        }
        return queryHandler;
    }

}