package com.kiss.template.simpledddcqrs.infrastructure.eventhandling.internal;

import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.EventHandler;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.EventHandlerInfo;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.IEventHandlerLookup;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.ReflectionUtils.MethodCallback;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Strategy to find event subscriptions made by a given object (the subscriber).
 * The event handling methods are considered to be the one declaring the
 * OrderProcessEventHandler annotation, and taking the event class as a
 * parameter.
 */
@Component
public class AnnotationEventHandlerLookup implements IEventHandlerLookup {

	@Override
	public Map<Class<?>, Set<EventHandlerInfo>> findAsynchronousHandlers(Object subscriber) {
		return findHandlers(subscriber, true);
	}

	public Map<Class<?>, Set<EventHandlerInfo>> findHandlers(Object subscriber, boolean synchronousFilter) {
		Map<Class<?>, Set<EventHandlerInfo>> handlers = new HashMap<>();
		for (Method m : getAllDeclaredMethods(subscriber.getClass())) {
			for (Annotation annotation : m.getAnnotations()) {
				if (annotation.annotationType().isAssignableFrom(EventHandler.class)) {
					if (((EventHandler) annotation).isAsync() == synchronousFilter) {
						Class<?> eventClass = m.getParameterTypes()[0];
						EventHandlerInfo eventHandlerInfo = new EventHandlerInfo(subscriber, m, ((EventHandler) annotation).isAsync());
						addHandler(handlers, eventClass, eventHandlerInfo);
					}
				}
			}
		}
		return handlers;
	}

	@Override
	public Map<Class<?>, Set<EventHandlerInfo>> findSynchronousHandlers(Object subscriber) {
		return findHandlers(subscriber, false);
	}

	private void addHandler(Map<Class<?>, Set<EventHandlerInfo>> subscriptions, Class<?> eventClass, EventHandlerInfo eventHandlerInfo) {
		Set<EventHandlerInfo> eventHandlers = subscriptions.get(eventClass);
		if (eventHandlers == null) {
			eventHandlers = new HashSet<>();
			subscriptions.put(eventClass, eventHandlers);
		}
		eventHandlers.add(eventHandlerInfo);
	}

	private Method[] getAllDeclaredMethods(Class<?> leafClass) throws IllegalArgumentException {
		final List<Method> list = new ArrayList<>(32);
		ReflectionUtils.doWithMethods(leafClass, method -> list.add(method));
		return list.toArray(new Method[list.size()]);
	}

}
