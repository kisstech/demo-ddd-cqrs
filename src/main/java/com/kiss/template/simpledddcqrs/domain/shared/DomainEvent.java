package com.kiss.template.simpledddcqrs.domain.shared;

import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;

/**
 * Base class for domain events raised by application modules.
 * 
 */
public abstract class DomainEvent extends Event {

	private static final long serialVersionUID = 8708734584468899823L;

	private static final Logger LOGGER = LoggerFactory.getLogger(DomainEvent.class);

	protected LocalDateTime dateTime = LocalDateTime.now();

	public void warnIfNullValue(Object value) {
		warnIfNullValue(value, "");
	}

	public void warnIfNullValue(Object value, String arg) {
		if (value == null) {
			LOGGER.warn("Domain event " + this.getClass().getName() + " was passed null value for argument " + arg);
		}
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}
}
