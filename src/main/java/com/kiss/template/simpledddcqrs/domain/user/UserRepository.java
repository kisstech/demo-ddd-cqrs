package com.kiss.template.simpledddcqrs.domain.user;

import java.util.List;

public interface UserRepository {

    /**
     * saves given user
     * @param user
     */
    User save(User user);

    List<User> findAll();

    User findByLogin(String login);
}
