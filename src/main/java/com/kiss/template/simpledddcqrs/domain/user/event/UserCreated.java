package com.kiss.template.simpledddcqrs.domain.user.event;

import com.kiss.template.simpledddcqrs.domain.shared.DomainEvent;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserCreated extends DomainEvent {
    private String firstname;
    private String lastname;
    private String email;
    private String login;
    private String password;
}
