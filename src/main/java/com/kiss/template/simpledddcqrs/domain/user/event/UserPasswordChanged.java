package com.kiss.template.simpledddcqrs.domain.user.event;

import com.kiss.template.simpledddcqrs.domain.shared.DomainEvent;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class UserPasswordChanged extends DomainEvent {

    private String login;
    private String password;

}
