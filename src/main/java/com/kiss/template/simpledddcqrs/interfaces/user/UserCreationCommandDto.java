package com.kiss.template.simpledddcqrs.interfaces.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserCreationCommandDto {

    private String firstname;
    private String lastname;
    private String email;
    private String login;
    private String password;

}
