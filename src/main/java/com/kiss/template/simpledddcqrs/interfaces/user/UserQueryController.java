package com.kiss.template.simpledddcqrs.interfaces.user;

import com.kiss.template.simpledddcqrs.application.user.query.UsersQuery;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Result;
import com.kiss.template.simpledddcqrs.interfaces.AbstractQueryController;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * query controller : read only, only GET method
 */
@RestController("userQueryController")
@RequestMapping("/api/users")
public class UserQueryController extends AbstractQueryController {

    @GetMapping
    public Result findAll() {
        return queryExecutor.execute(UsersQuery.builder().build());
    }
}
