package com.kiss.template.simpledddcqrs.interfaces.user;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserChangePasswordCommandDto {

    private String login;
    private String password;

}
