package com.kiss.template.simpledddcqrs.interfaces.admin.query;

import com.kiss.template.simpledddcqrs.application.admin.query.CommandListQuery;
import com.kiss.template.simpledddcqrs.application.admin.query.EventListQuery;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Result;
import com.kiss.template.simpledddcqrs.interfaces.AbstractQueryController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Demo controller to display all commands or events
 */
@RestController("adminQueryController")
@RequestMapping("/admin")
public class AdminQueryController extends AbstractQueryController {

    @GetMapping("/app/commands")
    public Result findCommands() {
        return queryExecutor.execute(CommandListQuery.builder().build());
    }

    @GetMapping("/app/events")
    public Result findEvents() {
        return queryExecutor.execute(EventListQuery.builder().build());
    }
}
