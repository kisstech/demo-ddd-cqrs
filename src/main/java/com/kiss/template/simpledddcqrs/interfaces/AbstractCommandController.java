package com.kiss.template.simpledddcqrs.interfaces;

import com.kiss.template.simpledddcqrs.application.common.command.CommandExecutor;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractCommandController {

    @Autowired
    protected CommandExecutor commandExecutor;

}
