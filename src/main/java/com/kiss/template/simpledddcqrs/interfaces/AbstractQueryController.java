package com.kiss.template.simpledddcqrs.interfaces;

import com.kiss.template.simpledddcqrs.application.common.query.QueryExecutor;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractQueryController {

    @Autowired
    protected QueryExecutor queryExecutor;
}
