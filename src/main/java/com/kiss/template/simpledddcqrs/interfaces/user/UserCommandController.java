package com.kiss.template.simpledddcqrs.interfaces.user;

import com.kiss.template.simpledddcqrs.application.user.command.ChangeUserPassword;
import com.kiss.template.simpledddcqrs.application.user.command.CreateUser;
import com.kiss.template.simpledddcqrs.interfaces.AbstractCommandController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Command controller : method PUT, POST, PATCH, DELETE
 * => changing application state
 */
@RestController("userCommandController")
@RequestMapping("/api/users")
public class UserCommandController extends AbstractCommandController {

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void create(@RequestBody UserCreationCommandDto userDto) {
        commandExecutor.execute(CreateUser.builder()
                .firstname(userDto.getFirstname())
                .lastname(userDto.getLastname())
                .email(userDto.getEmail())
                .login(userDto.getLogin())
                .password(userDto.getPassword())
                .build());

    }

    @PostMapping(value = "password", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void changePassword(@RequestBody UserChangePasswordCommandDto userDto) {
        commandExecutor.execute(ChangeUserPassword.builder()
                .login(userDto.getLogin())
                .password(userDto.getPassword())
                .build());
    }
}
