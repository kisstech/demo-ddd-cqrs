package com.kiss.template.simpledddcqrs.application.user.command;

import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.Command;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class CreateUser extends Command {
    private String firstname;
    private String lastname;
    private String email;
    private String login;
    private String password;
}
