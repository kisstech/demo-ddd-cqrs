package com.kiss.template.simpledddcqrs.application.user.query;

import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Query;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Result;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class UsersQuery extends Query<UsersQuery.UsersQueryResult> {

    @Builder
    @Getter
    public static class UsersQueryResult extends Result {
        private List<UserDto> users;
    }

    @Builder
    @Getter
    public  static class UserDto{
        private String fullname;
        private String email;
    }
}
