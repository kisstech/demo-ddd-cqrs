package com.kiss.template.simpledddcqrs.application.user.internal;

import com.kiss.template.simpledddcqrs.domain.user.event.UserCreated;
import com.kiss.template.simpledddcqrs.domain.user.event.UserPasswordChanged;
import com.kiss.template.simpledddcqrs.domain.user.User;
import com.kiss.template.simpledddcqrs.domain.user.UserRepository;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.EventHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserEventHandler {

    @Autowired
    private UserRepository repository;

    @EventHandler
    public void handle(UserCreated event) {
        //in case event sourcing is not implemented
        repository.save(User.builder()
                .firstname(event.getFirstname())
                .lastname(event.getLastname())
                .email(event.getEmail())
                .login(event.getLogin())
                .password(event.getPassword())
                .build());
    }

    @EventHandler
    public void handle(UserPasswordChanged event) {
        User user = repository.findByLogin(event.getLogin());
        user.changePassword(event.getPassword());
        repository.save(user);
    }

}
