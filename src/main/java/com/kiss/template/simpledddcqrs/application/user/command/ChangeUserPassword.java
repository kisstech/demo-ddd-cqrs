package com.kiss.template.simpledddcqrs.application.user.command;

import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.Command;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ChangeUserPassword extends Command {
    private String login;
    private String password;
}
