package com.kiss.template.simpledddcqrs.application.user.internal;


import com.kiss.template.simpledddcqrs.application.user.query.UsersQuery;
import com.kiss.template.simpledddcqrs.application.user.query.UsersQuery.UsersQueryResult;
import com.kiss.template.simpledddcqrs.domain.user.UserRepository;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.AbstractQueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.stream.Collectors;

@Service
public class UsersQueryHandler extends AbstractQueryHandler<UsersQuery, UsersQueryResult> {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UsersQueryResult handle(UsersQuery query) {
        return UsersQueryResult.builder()
                .users(userRepository.findAll().stream()
                        .map(u -> UsersQuery.UserDto.builder()
                                .email(u.getEmail())
                                .fullname(u.getFirstname()+" "+u.getLastname())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    @Override
    public Class<?> getQueryType() {
        return UsersQuery.class;
    }
}
