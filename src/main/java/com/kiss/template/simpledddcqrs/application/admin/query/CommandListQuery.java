package com.kiss.template.simpledddcqrs.application.admin.query;

import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Query;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Result;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CommandListQuery extends Query<CommandListQuery.CommandListQueryResult> {

    @Getter
    @Builder
    public static class CommandListQueryResult extends Result {
        private List<CommandDto> commands;
    }

    @Getter
    @Builder
    public static class CommandDto {
        private String name;
        private Object payload;
    }

}
