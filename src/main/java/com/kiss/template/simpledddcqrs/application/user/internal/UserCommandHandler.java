package com.kiss.template.simpledddcqrs.application.user.internal;

import com.kiss.template.simpledddcqrs.application.common.command.AbstractCommandHandlers;
import com.kiss.template.simpledddcqrs.application.user.command.ChangeUserPassword;
import com.kiss.template.simpledddcqrs.application.user.command.CreateUser;
import com.kiss.template.simpledddcqrs.domain.user.event.UserCreated;
import com.kiss.template.simpledddcqrs.domain.user.event.UserPasswordChanged;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.CommandHandler;
import org.springframework.stereotype.Service;

@Service
public class UserCommandHandler extends AbstractCommandHandlers {

    @CommandHandler
    public void handle(CreateUser cmd) {
        notifyThat(UserCreated.builder()
                .firstname(cmd.getFirstname())
                .lastname(cmd.getLastname())
                .email(cmd.getEmail())
                .login(cmd.getLogin())
                .password(cmd.getPassword())
                .build());
    }

    @CommandHandler
    public void handle(ChangeUserPassword cmd) {
        notifyThat(UserPasswordChanged.builder()
                .login(cmd.getLogin())
                .password(cmd.getPassword())
                .build());
    }
}
