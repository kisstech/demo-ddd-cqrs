package com.kiss.template.simpledddcqrs.application.common.event;

import com.kiss.template.simpledddcqrs.domain.shared.DomainEvent;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.IEventBus;
import com.kiss.template.simpledddcqrs.infrastructure.eventhandling.IEventInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DomainEventBus {

    @Autowired
    protected IEventBus eventBus;

    @Resource
    private List<IEventInterceptor> eventInterceptors;

    public void notifyThat(DomainEvent event) {
        for (IEventInterceptor interceptor : eventInterceptors) {
            interceptor.handle(event);
        }
        eventBus.publish(event);
    }

    public void subscribe(Object bean) {
        eventBus.subscribe(bean);
    }

}
