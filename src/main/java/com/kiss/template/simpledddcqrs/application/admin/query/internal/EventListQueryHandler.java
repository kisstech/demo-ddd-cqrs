package com.kiss.template.simpledddcqrs.application.admin.query.internal;


import com.kiss.template.simpledddcqrs.application.admin.query.EventListQuery;
import com.kiss.template.simpledddcqrs.application.admin.query.EventListQuery.EventListQueryResult;
import com.kiss.template.simpledddcqrs.infrastructure.logging.AppLog;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.AbstractQueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventListQueryHandler extends AbstractQueryHandler<EventListQuery, EventListQueryResult> {

    @Autowired
    private AppLog appLog;

    @Override
    public EventListQueryResult handle(EventListQuery query) {
        List<EventListQuery.EventDto> events = appLog.getEvents().stream()
                .map(c -> EventListQuery.EventDto.builder().name(c.getClass().getName()).payload(c).build())
                .collect(Collectors.toList());
        return EventListQueryResult.builder()
                .events(events)
                .build();
    }

    @Override
    public Class<?> getQueryType() {
        return EventListQuery.class;
    }
}
