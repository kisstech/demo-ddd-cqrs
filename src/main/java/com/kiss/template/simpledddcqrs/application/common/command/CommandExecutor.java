package com.kiss.template.simpledddcqrs.application.common.command;

import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.Command;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.CommandHandlingException;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.ICommandDispatcher;
import com.kiss.template.simpledddcqrs.infrastructure.commandhandling.ICommandInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

@Service
public class CommandExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommandExecutor.class);

    @Autowired
    private ICommandDispatcher commandDispatcher;

    @Resource
    private List<ICommandInterceptor> commandInterceptors;

    public void execute(Command command) {
        try {
            for (ICommandInterceptor interceptor : commandInterceptors) {
                interceptor.handle(command);
            }
            commandDispatcher.dispatch(command);
        } catch (CommandHandlingException e) {
            LOGGER.error("Unknown exception thrown", e);
            throw new RuntimeException(e.getCause());
        }
    }

    public void register(Object bean) {
        commandDispatcher.register(bean);
    }

    public Set<Class<?>> getCommands() {
        return commandDispatcher.getCommands();
    }

}
