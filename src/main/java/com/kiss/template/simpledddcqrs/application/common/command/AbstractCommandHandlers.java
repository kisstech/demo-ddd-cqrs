package com.kiss.template.simpledddcqrs.application.common.command;

import com.kiss.template.simpledddcqrs.domain.shared.DomainEvent;
import com.kiss.template.simpledddcqrs.application.common.event.DomainEventBus;
import org.springframework.beans.factory.annotation.Autowired;


public abstract class AbstractCommandHandlers {

    @Autowired
    private DomainEventBus domainEventBus;

    protected void notifyThat(DomainEvent event) {
        domainEventBus.notifyThat(event);
    }

}
