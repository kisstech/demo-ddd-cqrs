package com.kiss.template.simpledddcqrs.application.internal;

import com.kiss.template.simpledddcqrs.application.common.command.CommandExecutor;
import com.kiss.template.simpledddcqrs.application.common.event.DomainEventBus;
import com.kiss.template.simpledddcqrs.application.common.query.QueryExecutor;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.AbstractQueryHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;

@Component
public class HandlersBeanPostProcessor implements DestructionAwareBeanPostProcessor {

	@Autowired
	protected DomainEventBus domainEventBus;

	@Autowired
	protected CommandExecutor commandExecutor;

	@Autowired
	protected QueryExecutor queryExecutor;

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if (isInAppLayer(bean)) {
			if (isService(bean)) {
				if (isQueryHandler(bean)) {
					queryExecutor.register((AbstractQueryHandler<?, ?>) bean);
				} else {
					commandExecutor.register(bean);
				}
			}
			domainEventBus.subscribe(bean);
		}
		return bean;
	}

	private boolean isQueryHandler(Object bean) {
		return bean instanceof AbstractQueryHandler;
	}

	private boolean isService(Object bean) {
		Annotation[] annotations = bean.getClass().getAnnotations();
		for (Annotation annotation : annotations) {
			if (annotation.annotationType().isAssignableFrom(Service.class)) {
				return true;
			}
		}
		return false;
	}

	private boolean isInAppLayer(Object bean) {
		String name = "" + bean.getClass().getCanonicalName();
		//app layer package
		return name.startsWith("com.kiss.template.simpledddcqrs.application");
	}

	@Override
	public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException {
	}

}
