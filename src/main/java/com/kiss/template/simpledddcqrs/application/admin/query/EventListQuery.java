package com.kiss.template.simpledddcqrs.application.admin.query;

import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Query;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Result;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class EventListQuery extends Query<EventListQuery.EventListQueryResult> {

    @Getter
    @Builder
    public static class EventListQueryResult extends Result {
        private List<EventDto> events;
    }

    @Getter
    @Builder
    public static class EventDto {
        private String name;
        private Object payload;
    }
}
