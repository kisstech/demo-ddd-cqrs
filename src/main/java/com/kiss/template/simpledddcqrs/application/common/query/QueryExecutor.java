package com.kiss.template.simpledddcqrs.application.common.query;

import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.IQueryDispatcher;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Query;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.AbstractQueryHandler;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class QueryExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueryExecutor.class);

    @Autowired
    private IQueryDispatcher queryDispatcher;

    public <Q extends Query<R>, R extends Result> R execute(Q query) {
        return queryDispatcher.dispatch(query);

    }

    public void register(AbstractQueryHandler<?, ?> handler) {
        queryDispatcher.register(handler);
    }

}
