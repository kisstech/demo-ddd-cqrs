package com.kiss.template.simpledddcqrs.application.admin.query.internal;


import com.kiss.template.simpledddcqrs.application.admin.query.CommandListQuery;
import com.kiss.template.simpledddcqrs.application.admin.query.CommandListQuery.CommandDto;
import com.kiss.template.simpledddcqrs.application.admin.query.CommandListQuery.CommandListQueryResult;
import com.kiss.template.simpledddcqrs.infrastructure.logging.AppLog;
import com.kiss.template.simpledddcqrs.infrastructure.queryhandling.AbstractQueryHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommandListQueryHandler extends AbstractQueryHandler<CommandListQuery, CommandListQueryResult> {

    @Autowired
    private AppLog appLog;

    @Override
    public CommandListQueryResult handle(CommandListQuery query) {
        List<CommandDto> commands = appLog.getCommands().stream()
                .map(c -> CommandDto.builder().name(c.getClass().getName()).payload(c).build())
                .collect(Collectors.toList());
        return CommandListQueryResult.builder()
                .commands(commands)
                .build();
    }

    @Override
    public Class<?> getQueryType() {
        return CommandListQuery.class;
    }
}
